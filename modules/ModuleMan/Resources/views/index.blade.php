@extends('moduleman::layouts.master')

@section('content')
<style>

    body {
        background-color: #e3e8ea;
    }

    .enable-btn {
        background: #48d900;
        background-image: -webkit-linear-gradient(top, #48d900, #166e00);
        background-image: -moz-linear-gradient(top, #48d900, #166e00);
        background-image: -ms-linear-gradient(top, #48d900, #166e00);
        background-image: -o-linear-gradient(top, #48d900, #166e00);
        background-image: linear-gradient(to bottom, #48d900, #166e00);
        -webkit-border-radius: 28;
        -moz-border-radius: 28;
        border-radius: 28px;
        font-family: Courier New;
        color: #ffffff;
        font-size: 14px;
        padding: 5px 10px 5px 10px;
        text-decoration: none;
    }

    .disabled {
        background: #fa4b4b;
        background-image: -webkit-linear-gradient(top, #fa4b4b, #b80000);
        background-image: -moz-linear-gradient(top, #fa4b4b, #b80000);
        background-image: -ms-linear-gradient(top, #fa4b4b, #b80000);
        background-image: -o-linear-gradient(top, #fa4b4b, #b80000);
        background-image: linear-gradient(to bottom, #fa4b4b, #b80000);
        -webkit-border-radius: 28;
        -moz-border-radius: 28;
        border-radius: 28px;
        font-family: Courier New;
        color: #ffffff;
        font-size: 14px;
        padding: 5px 10px 5px 10px;
        text-decoration: none;
    }

    .enabled {
        background: #17ff3a;
        background-image: -webkit-linear-gradient(top, #17ff3a, #00ab36);
        background-image: -moz-linear-gradient(top, #17ff3a, #00ab36);
        background-image: -ms-linear-gradient(top, #17ff3a, #00ab36);
        background-image: -o-linear-gradient(top, #17ff3a, #00ab36);
        background-image: linear-gradient(to bottom, #17ff3a, #00ab36);

        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;

        padding: 2.5px 10px;
        margin: 10px 10px;
    }

    .enable-btn:hover {
        background: #17ff3a;
        background-image: -webkit-linear-gradient(top, #17ff3a, #00ab36);
        background-image: -moz-linear-gradient(top, #17ff3a, #00ab36);
        background-image: -ms-linear-gradient(top, #17ff3a, #00ab36);
        background-image: -o-linear-gradient(top, #17ff3a, #00ab36);
        background-image: linear-gradient(to bottom, #17ff3a, #00ab36);
        text-decoration: none;
    }

    .disable {
        background: #fa4b4b;
        background-image: -webkit-linear-gradient(top, #fa4b4b, #b80000);
        background-image: -moz-linear-gradient(top, #fa4b4b, #b80000);
        background-image: -ms-linear-gradient(top, #fa4b4b, #b80000);
        background-image: -o-linear-gradient(top, #fa4b4b, #b80000);
        background-image: linear-gradient(to bottom, #fa4b4b, #b80000);

        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;

        padding: 2.5px 10px;
        margin: 10px 10px;
    }

    .disable-btn {
        background: #fa4b4b;
        background-image: -webkit-linear-gradient(top, #fa4b4b, #b80000);
        background-image: -moz-linear-gradient(top, #fa4b4b, #b80000);
        background-image: -ms-linear-gradient(top, #fa4b4b, #b80000);
        background-image: -o-linear-gradient(top, #fa4b4b, #b80000);
        background-image: linear-gradient(to bottom, #fa4b4b, #b80000);
        -webkit-border-radius: 28;
        -moz-border-radius: 28;
        border-radius: 28px;
        font-family: Courier New;
        color: #ffffff;
        font-size: 14px;
        padding: 5px 10px 5px 10px;
        text-decoration: none;
    }

    .disable-btn:hover {
        background: #f51862;
        background-image: -webkit-linear-gradient(top, #f51862, #9e0337);
        background-image: -moz-linear-gradient(top, #f51862, #9e0337);
        background-image: -ms-linear-gradient(top, #f51862, #9e0337);
        background-image: -o-linear-gradient(top, #f51862, #9e0337);
        background-image: linear-gradient(to bottom, #f51862, #9e0337);
        text-decoration: none;
    }

    .list {
        display: block;
        margin: 10px 10px;
        padding: 10px;
        border-radius: 1px;
        -webkit-border-radius: 1px;
        -moz-border-radius: 1px;
        background-color: #f8f8f8;

        -webkit-box-shadow: 0 10px 6px -6px #777;
        -moz-box-shadow: 0 10px 6px -6px #777;
        box-shadow: 0 10px 6px -6px #777;
    }

    .alias {
        background-color: #ebccd1;
        padding: 0px 10px;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;

        padding: 2.5px 10px;
        margin: 10px 10px;

    }

    /*
    * CSS Notification
    */
    div.notification-fail {
        -webkit-animation: seconds 1.0s forwards;
        -webkit-animation-iteration-count: 1;
        -webkit-animation-delay: 5s;
        animation: seconds 1.0s forwards;
        animation-iteration-count: 1;
        animation-delay: 5s;


        background: red;
        margin-bottom: 10px;


        padding: 10px;
        text-align: center;
        font-size: 18px;
        color: white;
    }

    div.notification-success {
        -webkit-animation: seconds 1.0s forwards;
        -webkit-animation-iteration-count: 1;
        -webkit-animation-delay: 5s;
        animation: seconds 1.0s forwards;
        animation-iteration-count: 1;
        animation-delay: 5s;


        background: green;
        

        padding: 10px;
        text-align: center;
        font-size: 18px;
        color: white;
    }


    @-webkit-keyframes seconds {
        0% {
            opacity: 1;
        }
        100% {
            opacity: 0;
        }
    }
    @keyframes seconds {
        0% {
            opacity: 1;
        }
        100% {
            opacity: 0;
        }
    }

    b.underline {
        text-decoration: underline;
    }

</style>

@if(Session::has('moduleManagerMsg'))
<div class="{!! Session::get('class') !!}">{!! Session::get('moduleManagerMsg') !!}</div>
@endif

<div style="float: left; width: 100%; margin-top: 25px;">
    <h1 style="text-align: center">Laravel Module Manager</h1>

    <div>Number of Installed Modules : {{ count($modules) }}</div>
@foreach ($modules as $module)
    <div class="list">
        <p>
            <span style="font-size: 28px;">{{ $module['name'] }}</span>
            <small class="alias">
                <i>alias => {{$module['alias']}}</i>
            </small>
            @if ($module['active'] == 1)
            <label class="enabled">Enabled</label>
            <span style="float: right; margin-top: 15px;">
                <a class="disable-btn" href="{{url('modulemanager/disable/'.$module['name']) }}">Disable</a>
            </span>
            @else
            <label class="disabled">Disabled</label>
            <span style="float: right; margin-top: 15px;">
                <a class="enable-btn" href="{{url('modulemanager/enable/'.$module['name']) }}">Enable</a>
            </span>
            @endif


        </p>

        <div>
            <small><code>{{ $module['description'] }}</code></small>
        </div>

        @if ($module['active'] == 1)
        <div>
            <small><b class="underline">Namespaces</b></small>
            @foreach ($module['namespaces'] as $name)
            <div>
                <small><code>{{ $name }}</code></small>
            </div>
            @endforeach
        </div>
        @else
        <div>
            <small class="error">No Namespaces</small>
        </div>
        @endif

        <div>
            <small><code><b class="underline">Path</b> - {{ $module['path'] }}</code></small>
        </div>

        <div>
            <small><code><b class="underline">Order</b> - {{ $module['order'] }}</code></small>
        </div>



        <div>
            <small><code><b class="underline">Config</b> - {{ $module['json'] }}</code></small>
        </div>



    </div>

@endforeach
</div>
<footer style="text-align: center">
    Generated from Laravel Module Manager {{ date('Y') }}
</footer>
@stop