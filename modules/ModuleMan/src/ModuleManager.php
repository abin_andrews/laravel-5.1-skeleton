<?php
/**
 * Created by PhpStorm.
 * User: abin
 * Date: 16/9/15
 * Time: 10:54 AM
 */

namespace Modules\Moduleman;

use Module;

class ModuleManager
{
    private $name;
    private $alias;
    private $description;
    private $keywords;
    private $active;
    private $order;
    private $provider;
    private $aliases;
    private $files;
    private $path;
    private $json;
    private $modulePtr;
    private $moduleNamespace;

    private static $managerModuleName = 'Moduleman';
    private static $namespaces = array();

    public function __construct($moduleName) {
            $module = (Object) self::getModuleDetail($moduleName);
            $this->name = $module->name;
            $this->alias = $module->alias;
            $this->description = $module->description;
            $this->keywords = $module->keywords;
            $this->active = $module->active;
            $this->order = $module->order;
            $this->provider = $module->provider;
            $this->aliases = $module->aliases;
            $this->files = $module->files; //Array
            $this->path = $module->path;
            $this->json = $module->json;
            $this->modulePtr = Module::find($module->alias);
    }

    public static function getModuleMangerName() {
        return self::$managerModuleName;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @return mixed
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @return mixed
     */
    public function getAliases()
    {
        return $this->aliases;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @return mixed
     */
    public function getJson()
    {
        return $this->json;
    }

    /**
     * @return mixed
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @return mixed
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * Enable current module
     * @return bool
     */
    public function enable() { return self::enableModule($this->name); }

    /**
     * Disbale current module
     * @return bool
     */
    public function disable() { return self::disableModule($this->name); }

    /**
     * Delete current module
     * @return bool
     */
    public function delete() {
        if(self::deleteModule($this->name)) {
            unset($this);
            return false;
        }
    }



    public static function getModuleDetail($moduleName) {
        $modulePath = Module::getModulePath($moduleName);
        $module = json_decode(file_get_contents($modulePath . 'module.json'));
        return array(
            "name" => $module->name,
            "alias" => $module->alias,
            "description" => $module->description,
            "keywords" => $module->keywords, //Array
            "active" => $module->active, //Integer
            "order" => $module->order, //Integer
            "provider" => $module->providers, //Array
            "aliases" => $module->aliases, //Array
            "files" => $module->files, //Array
            "path" => $modulePath,
            "json" => $modulePath . 'module.json',
        );
    }

    public static function buildModuleList()
    {
        //To get the list of all modules
        $obj = Module::all();
        $moduleList = array();

        //Building the modulelist array
        foreach ($obj as $key => $val) {
            $moduleList[$key] = self::getModuleDetail($key);
            if($moduleList[$key]['active'] == 1) {
                $moduleList[$key]['namespaces'] = self::getNamespaces($key, true);
            }
        }
        return $moduleList;
    }

    private static function checkModule($moduleName) {
        if(Module::has($moduleName)) {
            if($moduleName == self::$managerModuleName) {
               return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    private static function getModuleInstance($moduleName){
        if(self::checkModule($moduleName))
            return Module::find($moduleName);
        else
            return false;
    }

    private static function checkModuleStatus($moduleName) {
        if(self::checkModule($moduleName)) {
            $module = self::getModuleInstance($moduleName);
            if($module->active == 1)
                return 1;
            else
                return 0;
        } else {
            return false;
        }
    }

    public static function deleteModule($moduleName) {
        $module = self::getModuleInstance($moduleName);
        if($module) {
            $module->delete();
            return true;
        } else {
            return false;
        }
    }


    public static function enableModule($moduleName) {
        $module = self::getModuleInstance($moduleName);
        if($module && self::checkModuleStatus($moduleName) == 0) {
            $module->enable();
            return true;
        }
        return false;
    }

    public static function disableModule($moduleName) {
        $module = self::getModuleInstance($moduleName);
        if($module && self::checkModuleStatus($moduleName) == 1) {
            $module->disable();
            return true;
        }
        return false;
    }

    /**
     * To get the details about the namespaces being used by the module manager
     * @return mixed array of enabled modules
     */
    private static function getModuleNamespaces($module) {
        $namespaces=array();
        foreach(get_declared_classes() as $name) {
            if(preg_match_all("@[^\\\]+(?=\\\)@iU", $name, $matches)) {
                if( ($matches[0][0] == "Modules") && ($matches [0][1] == $module)) {
                    $namespaces[] = $name;
                }
            }
        }
        return $namespaces;
    }

    public static function getNamespaces($moduleName, $byPassCheck = false) {
        if($byPassCheck) {
            return self::getModuleNamespaces($moduleName);
        }
        else {
            if(self::checkModule($moduleName)) {
                return self::getModuleNamespaces($moduleName);
            }
        }
    }
}