<?php

Route::group(['prefix' => 'modulemanager', 'namespace' => 'Modules\ModuleMan\Http\Controllers'], function()
{
	Route::get('/',
        [
        'as' => 'moduleman',
        'uses' => 'ModuleManController@index'
        ]);

    Route::get('/enable/{id}', 'ModuleManController@enabled');

    Route::get('/disable/{id}', 'ModuleManController@disabled');

    Route::get('/view', 'ModuleManController@view');
});