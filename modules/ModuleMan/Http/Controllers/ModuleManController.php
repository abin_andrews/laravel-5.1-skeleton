<?php namespace Modules\Moduleman\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Pingpong\Modules\Routing\Controller;
use Input;
use Module;
use Session;
use Modules\Moduleman\ModuleManager;
use Illuminate\Support\Facades\Redirect;

class ModuleManController extends Controller {

	public function index()
	{
        $modules = ModuleManager::buildModuleList();
        //dd($modules);
        return view('moduleman::index', ['modules' => $modules]);
    }

    public function view()
    {
        dd(ModuleManager::getNamespaces("Dummy2"));


//        function test($arr, $share) {
//            foreach($arr as $key => $value) {
//                $share .="\\".$key;
//                echo $share;
//                echo "<br>";
//                if(is_array($value)) {
//                    test($value, $share);
//                } else {
//                    if(strlen($value) != 0) {
//                        $share .="\\".$value;
//                        echo $share;
//                        echo "Here";
//
//                    }
//                    //echo $value;
//                }
//            }
//            return $share;
//        }




        // loop through the object
        // we need to create a RecursiveIteratorIterator instance


        //dd($arr);







        //return Redirect::action('\Modules\ModuleMan\Http\Controllers\ModuleManController@index');

       /* $routeCollection = \Route::getRoutes();
        $routes = array();

        $routes = $routeCollection->getByAction('Modules\ModuleMan\Http\Controllers\ModuleManController@index');
        foreach ($routeCollection as $value) {
//            echo $value->getPath();
//            echo $value->getUri();

            echo $value->getPath()."<br/>";
        }

        dd($routes);
        //return Redirect::route('moduleman');
        //dd(URL::setRootControllerNamespace('Modules\Moduleman\Http\Controllers')->action('ModuleManController@index'));

//        return Redirect::to('modulemanager');
//        return Redirect::action('ModuleMan::index');*/
    }

    public function enabled(Request $request, $id)
    {
        $module = new ModuleManager($id);
        if($module->enableModule($module->getName())) {
            Session::flash('class', 'notification-success');
            Session::flash('moduleManagerMsg', '<b>'.$module->getName().'</b> - Module enabled : Success.');
            return Redirect::action('\Modules\ModuleMan\Http\Controllers\ModuleManController@index');
        } else {
            Session::flash('class', 'notification-fail');
            Session::flash('moduleManagerMsg', '<b>'.$module->getName().'</b> - Module enabled : Fail');
            return Redirect::action('\Modules\ModuleMan\Http\Controllers\ModuleManController@index');
        }
    }

    public function disabled(Request $request, $id)
    {
        $module = new ModuleManager($id);
        if($module->disableModule($module->getName())) {
            Session::flash('class', 'notification-success');
            Session::flash('moduleManagerMsg', '<b>'.$module->getName().'</b> - Module diasabled : Success');
            return Redirect::action('\Modules\ModuleMan\Http\Controllers\ModuleManController@index');
        } else {
            Session::flash('class', 'notification-fail');
            if($id == ModuleManager::getModuleMangerName()) {
                Session::flash('moduleManagerMsg', '<b>'.$module->getName().'</b> - Default Moduel cannot be disabled');
                return Redirect::action('\Modules\ModuleMan\Http\Controllers\ModuleManController@index');
            }
            Session::flash('moduleManagerMsg', '<b>'.$module->getName().'</b> - Module diasabled : Fail');
            return Redirect::action('\Modules\ModuleMan\Http\Controllers\ModuleManController@index');
        }
    }

    public function enable()
    {

    }

    public function disable()
    {

    }

    public function delete()
    {

    }
}