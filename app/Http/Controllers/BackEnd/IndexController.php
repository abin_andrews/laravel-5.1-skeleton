<?php
/**
 * Created by PhpStorm.
 * User: abin
 * Date: 15/9/15
 * Time: 11:00 AM
 */

namespace App\Http\Controllers\BackEnd;

use App\Http\Controllers\BackEnd\BaseBackendController;


class IndexController extends BaseBackendController {

    public function testCont() {
        return view('backend/index');
    }

} 