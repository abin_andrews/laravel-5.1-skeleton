<!DOCTYPE html>
<html>
<head>
    <title>Laravel</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            display: table;
            font-weight: 100;
            font-family: 'Lato';
        }

        .container {
            text-align: center;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .title {
            font-size: 96px;
        }

        .form-label {
            font-size: 24px;
            font-style: normal;
            font-weight: 700;
        }

        .form-container {
            float: left;
        }

        .form-row {
            display: block;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="content">
        <div class="title">Admin Area</div>
        <div class="form-container">
            <form>
                <div class="form-row">
                    <label class="form-label">Enter User Name</label>
                    <input type="text" name="username">
                </div>
                <div class="form-row">
                    <label class="form-label">Enter Password</label>
                    <input type="password" name="password">
                </div>
                <div class="form-row">
                    <input type="submit" value="Login">
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
